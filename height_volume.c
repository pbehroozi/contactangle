//Contact Angle Calculator
//Code: Copyright (C)2017 Peter Behroozi, Feredoon Behroozi
//License: GNU GPLv3

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <math.h>

//CGS Units
#define SIGMA 72.0     //Surface tension
#define RHO   1.0      //Drop density
#define G     980.665  //Gravitational acceleration

#define NUM_STEPS 31415  //# of integration steps over 180 degrees


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif /* M_PI */
#define dtheta (M_PI / (double) NUM_STEPS)  //step size in radians


//Determines x(theta)->xa and y(theta)->ya
//Integrates via midpoint rule (leapfrog) to reduce errors
//Inputs are fluid parameters (sigma, rho, g), and the initial radius of curvature r0.
void calc_xy(double sigma, double g, double rho, double r0,   //Inputs
	     double *xa, double *ya) {                        //Outputs
  double theta = 0;
  double cost = cos(theta);
  double sint = sin(theta);
  double x=0, y=0;
  double denom; 

  //Initial values of dx/dtheta, dy/dtheta
  double dxdt = r0;
  double dydt=0;
  
  int64_t i;
  for (i=0; i<NUM_STEPS; i++) {
    //Store x(theta) and y(theta) in output arrays
    xa[i] = x;
    ya[i] = y;

    //Calculate x,y at theta + dtheta/2
    x += dxdt * dtheta / 2.0;
    y += dydt * dtheta / 2.0;
    theta += dtheta / 2.0;

    //Calculate dx/dt and dy/dt at theta+dtheta/2
    cost = cos(theta);
    sint = sin(theta);
    denom = 1.0/(((2.0*sigma / r0) + rho*g*y) - sigma*sint / x);
    dxdt = sigma * cost * denom;
    dydt = sigma * sint * denom;

    //Compute x+x'(theta+dtheta/2)*dtheta and same for y
    x = xa[i] + dxdt * dtheta;
    y = ya[i] + dydt * dtheta;    
    theta += dtheta / 2.0;    

    /*Recalculate dx/dt and dy/dt for next iteration*/
    cost = cos(theta);
    sint = sin(theta);
    denom = 1.0/(((2.0*sigma / r0) + rho*g*y) - sigma*sint / x);
    dxdt = sigma * cost * denom;
    dydt = sigma * sint * denom;
  }
}

//Calculates the footprint radius R given the fluid parameters (sigma, rho, g), the initial radius of curvature r0,
//and the height h.  Also calculates the contact angle theta and the mass enclosed.
int64_t calc_R_at_h(double sigma, double rho, double g, double r0, double h,    //Inputs
		    double *R, double *theta, double *mass) {                   //Outputs
  //Calculate drop profile in xa and ya:
  double xa[NUM_STEPS];
  double ya[NUM_STEPS];
  calc_xy(sigma, g, rho, r0, xa, ya);

  double m = 0;  //Mass enclosed

  //Step through theta values until y(theta) > h
  int64_t i;
  for (i=0; i<NUM_STEPS; i++) {
    //Also have to integrate mass enclosed, again using the midpoint rule
    double dy = 0;
    if (i>0) dy = (ya[i]-ya[i-1])/2.0;
    m += xa[i]*xa[i]*dy;   //We calculate the integral of (x^2 dy), scaling by pi*rho later.

    if (ya[i] > h) break;

    //Part 2 of midpoint rule:
    if (i<NUM_STEPS-1) dy = (ya[i+1]-ya[i])/2.0;
    m += xa[i]*xa[i]*dy;
  }

  //If we reached the end of the array (theta=pi) without finding
  //y(theta)>h, then there is no suitable answer
  if (i==NUM_STEPS) return -1; 

  //If we were given h=0, then return correct values
  if (i==0) { *R = *theta = *mass = 0; return 0; }

  //Since ya[i] > h, ya[i-1] must be <= h
  //We can then interpolate between the two steps
  //to determine R, theta, and mass more exactly.
  i--;
  double f = (h - ya[i])/(ya[i+1]-ya[i]); //Interpolation fraction

  *R = xa[i] + f*(xa[i+1]-xa[i]);
  *theta = ((double)i+f)*dtheta;

  //For mass:
  double dy = f*(ya[i+1]-ya[i]);    //Additional vertical height
  double avg_R = 0.5*(xa[i]+(*R));  //Average horizontal extent
  m += avg_R*avg_R*dy;              //Additional mass
  *mass = M_PI*m*rho;               //M_PI = pi
  return 0;
}

int main(void) {
  double sigma = SIGMA, rho = RHO, g = G;

  double h, R, mm;
  printf("Enter drop height (cm): ");
  scanf("%lf", &h);  //Get height from user input

  printf("Enter drop volume (mL): ");
  scanf("%lf", &mm);  

  //Start by guessing r0 = h 
  double r0 = h; //Crown radius of curvature

  //Apply Newton's method to solve for r0
  double m, theta, dm;
  calc_R_at_h(sigma, rho, g, r0, h, &R, &theta, &m); //Initial guess results

  int64_t count = 0;    //Iteration count
  double dr0 = r0/10.0; //step size

  //Iterate until our guess converges to the right answer (if possible)
  while ((fabs(m-mm) > 1e-6*mm) && count < 30) {
    count++;

    //See how R changes if we change r0
    int64_t ret = calc_R_at_h(sigma, rho, g, r0+dr0, h, &R, &theta, &dm);
    if (ret < 0) { dr0 /= 2.0; continue; } //If invalid parameters, try reducing step size
    dm -= m;

    //Calculate the best amount to move r0 via estimating dr0/dR
    //and multiplying by the difference between the right answer for R and the
    //result for R from our guess for r0 (i.e., R2).
    double move = (mm-m) * dr0 / dm;
    r0 += move;

    //Reduce the step size, since we should be closer to the right answer
    dr0 /= 2.0;

    //Calculate the (hopefully) improved guess for R2
    ret = calc_R_at_h(sigma, rho, g, r0, h, &R, &theta, &m);

    //If this results in invalid parameters, reduce the step size and try again
    if (ret < 0) { r0 -= move; dr0 /= 2.0; continue; }
  }

  if (count==30) {
    printf("Unsolvable, sorry.\n");
    exit(EXIT_FAILURE);
  }

  //Display solution results
  printf("height: %f cm\nMass: %f grams\nContact Angle: %f deg\nCrown Radius of Curvature: %f cm\nMass verification: %f grams\nFootprint radius: %f cm\n", h, mm, theta*180.0/M_PI, r0, m, R);
  return 0;
}
