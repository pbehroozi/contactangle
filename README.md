# Contact Angle Calculator

Code: Copyright (C)2017 Peter Behroozi, Feredoon Behroozi

License: GNU GPLv3

This code implements the contact angle calculation algorithm from a drop's height and volume given in Behroozi & Behroozi (2017).

If you have a C compiler installed, download the source, run `make` and then `./height_volume` from a terminal.  Otherwise, search for an online C compiler (e.g., [this one](https://www.onlinegdb.com/online_c_compiler)), paste the contents of `height_volume.c` (accessible via the "Source" tab on the left) and follow the compiler's instructions to run.

This code assumes properties for water (i.e., surface tension = 72 dyne/cm and density = 1 g/cm^3) at the surface of the earth (g = 980.665 cm/s^2).  For other circumstances, modify the constants at the beginning of `height_volume.c` appropriately.