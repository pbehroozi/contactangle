all:
	cc -O3 height_volume.c -o height_volume

dist:
	cd ..; zip contact_angle.zip ContactAngle/height_volume.c ContactAngle/Makefile ContactAngle/README.txt; mv contact_angle.zip ContactAngle
